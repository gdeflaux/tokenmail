class AddAliasOfToLogins < ActiveRecord::Migration[5.2]
  def change
    add_column :logins, :alias_of, :string
  end
end
