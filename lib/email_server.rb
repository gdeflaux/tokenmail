module EmailServer

  class Miab
    @@base_url = "https://box.ouechbien.fr/admin/mail"

    def self.alias_add(address, forwards_to)
      HTTP.basic_auth(:user => Rails.application.credentials.miab[:username], :pass => Rails.application.credentials.miab[:password]).post("#{@@base_url}/aliases/add", form: { address: address, forwards_to: forwards_to })
    end

    def self.alias_remove(address)
      HTTP.basic_auth(:user => Rails.application.credentials.miab[:username], :pass => Rails.application.credentials.miab[:password]).post("#{@@base_url}/aliases/remove", form: { address: address })
    end

    def self.emails_get
      HTTP.basic_auth(:user => Rails.application.credentials.miab[:username], :pass => Rails.application.credentials.miab[:password]).get("#{@@base_url}/users")
    end
  end

end
