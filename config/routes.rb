Rails.application.routes.draw do
  resources :logins

  get "generate_password", to: "logins#generate_password"
  get "generate_username", to: "logins#generate_username"

  root "logins#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
