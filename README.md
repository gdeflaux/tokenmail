# TokenMail

A password manager that creates a new email address for each website/login. Works in tandem with [Mail In A Box](https://mailinabox.email/).

**Why is this useful?**

Email addresses are considered to be _stable_ unique identifier that allow service to track your online activity on the internet and profile you, compromising your privacy. By using a different email address for every website / online service you sign up for, you reduce the tracking risk and the profiling.

## Documentation

### Frameworks

- [Bootstrap](https://getbootstrap.com/docs/4.4/getting-started/introduction/)
- [JQuery](https://api.jquery.com/)
- [Fontawesome](https://fontawesome.com/icons)

### Gems

- [slim-rails](https://github.com/slim-template/slim-rails)
- [faker](https://github.com/faker-ruby/faker)
- [http](https://github.com/httprb/http)
- [bootstrap](https://github.com/twbs/bootstrap-rubygem)
- [font_awesome5_rails](https://github.com/tomkra/font_awesome5_rails)
- [bootstrap_form](https://github.com/bootstrap-ruby/bootstrap_form)
- [jquery-rails](https://github.com/rails/jquery-rails)

### Postgres

#### Install & Configure Postgres on Ubuntu For Rails

```
$ sudo apt-get install postgresql
$ sudo apt-get install libpq-dev
$ sudo su postgres -c psql
postgres=# CREATE ROLE <username> SUPERUSER LOGIN;
postgres=# ALTER ROLE <username> WITH PASSWORD '<password>';
postgres=# \q
```

#### Reset Heroku Database

This operation will **delete** the production database.

```
$ heroku pg:reset DATABASE_URL
$ heroku run rails db:migrate
$ heroku run rails db:seed
```
### Credentials

Create _credentials_ with the MIAB username and password

```
EDITOR=nano rails credentials:edit
```

Edit the content of the file

```
miab:
  username: <miab username>
  password: <miab password>
```

Set the master key as an Heroku environment variable:

```
heroku config:set RAILS_MASTER_KEY=<content of config/master.key>
```
