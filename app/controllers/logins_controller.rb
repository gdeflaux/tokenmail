class LoginsController < ApplicationController
  before_action :set_login, only: [:show, :edit, :update, :destroy]

  require 'email_server'

  # GET /logins
  # GET /logins.json
  def index
    @logins = Login.all
  end

  # GET /logins/1
  # GET /logins/1.json
  def show
  end

  # GET /logins/new
  def new
    @login = Login.new
    @login.username = Login.generate_username
    @login.password = Login.generate_password

    @email_accounts = get_email_accounts
  end

  # GET /logins/1/edit
  def edit
    @email_accounts = get_email_accounts
  end

  # POST /logins
  # POST /logins.json
  def create
    @login = Login.new(login_params)

    try_saving = @login.valid?

    if (try_saving && @login.alias_of != "")
      resp = EmailServer::Miab.alias_add(@login.username, @login.alias_of)
      if resp.code == 200
        try_saving = true
      else
        try_saving = false
        @login.errors.add(:alias_of, "Couldn't create the alias on email server.")
      end
    end

    respond_to do |format|
      if (try_saving && @login.save)
        format.html { redirect_to @login, notice: 'Login was successfully created.' }
        format.json { render :show, status: :created, location: @login }
      else
        @login.assign_attributes(login_params)
        @email_accounts = get_email_accounts
        format.html { render :new }
        format.json { render json: @login.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /logins/1
  # PATCH/PUT /logins/1.json
  def update
    test_login = Login.new(login_params)
    try_updating = test_login.valid?
    if !try_updating
      @login.errors.merge!(test_login.errors)
      @login.assign_attributes(login_params)
    end

    # had alias | no alias => destroy old
    # had alias | different alias => destroy old AND create new
    # no alias | has alias => create new
    username_change = @login.username != login_params[:username]

    # alias of change => destroy old AND create new
    alias_of_change = (@login.alias_of != "") && (login_params[:alias_of] != "") && (@login.alias_of != login_params[:alias_of])

    if try_updating
      err = []
      if (username_change && @login.alias_of != "") || alias_of_change
        resp = EmailServer::Miab.alias_remove(@login.username)
        if resp.code != 200
          err << "Failed to remove old alias on email server"
        end
      end
      if (username_change && login_params[:alias_of] != "") || alias_of_change
        resp = EmailServer::Miab.alias_add(login_params[:username], login_params[:alias_of])
        if resp.code != 200
          err << "Failed to create new alias on email server"
        end
      end
      if err.length > 0
        try_updating = false
        @login.errors.add(:alias_of, err.join(", "))
      end
    end

    respond_to do |format|
      if try_updating && @login.update(login_params)
        format.html { redirect_to @login, notice: 'Login was successfully updated.' }
        format.json { render :show, status: :ok, location: @login }
      else
        @login.assign_attributes(login_params)
        @email_accounts = get_email_accounts
        format.html { render :edit }
        format.json { render json: @login.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /logins/1
  # DELETE /logins/1.json
  def destroy
    alias_deleted = true
    if @login.alias_of != ""
      resp = EmailServer::Miab.alias_remove(@login.username)
      alias_deleted = (resp.code == 200)
    end

    respond_to do |format|
      if alias_deleted
        @login.destroy
        format.html { redirect_to logins_url, notice: 'Login was successfully destroyed.' }
        format.json { head :no_content }
      else
        format.html { redirect_to logins_url, alert: 'Failed to delete alias on th email server.' }
        format.json { head :no_content }
      end
    end
  end

  def generate_username
    respond_to do |format|
      format.json do
        @username = Login.generate_username
      end
    end
  end

  def generate_password
    respond_to do |format|
      format.json do
        @pwd = Login.generate_password
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_login
      @login = Login.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def login_params
      params.require(:login).permit(:name, :username, :password, :url, :alias_of)
    end

    def get_email_accounts
      resp = EmailServer::Miab.emails_get
      resp.body.to_s.split("\n").map { |e| [e] }
    end
end
