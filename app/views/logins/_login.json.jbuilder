json.extract! login, :id, :name, :username, :password, :url, :created_at, :updated_at
json.url login_url(login, format: :json)
