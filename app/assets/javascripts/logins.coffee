# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->

  $("#generatePassword").on "ajax:success", (event) ->
    [data, status, xhr] = event.detail
    $("#login_password").val(data.pwd)

  $("#generateUsername").on "ajax:success", (event) ->
    [data, status, xhr] = event.detail
    alias = $("#login_alias_of").val()
    username = data.username
    update_username(alias, data.username)

  $("#login_alias_of").change ->
    alias = $(this).val()
    username = $("#login_username").val()
    update_username(alias, username)

  update_username = (alias, username) ->
    if alias == ""
      username = username.split("@")[0]
    else
      domain = alias.match(/@.*$/i)[0]
      if username.includes("@")
        username = username.split("@")[0]
      username = username.concat(domain)
    $("#login_username").val(username)
