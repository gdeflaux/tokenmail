class Login < ApplicationRecord
    validates :name, :username, :password, :url, presence: true

    def self.generate_username (words = 3)
      Faker::Name.first_name.downcase + "." + Faker::Verb.simple_present.downcase + "." + Faker::Creature::Animal.name.downcase
    end

    def self.generate_password(len = 12)
      Faker::Internet.password(min_length: len)
    end
end
